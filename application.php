#!/usr/bin/env php
<?php

require __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Console\Application;
use Dockify\LaravelCreator\Commands\RunCommand;
use Dockify\LaravelCreator\Commands\LeaveCommand;
use Dockify\LaravelCreator\Commands\InstallCommand;

$application = new Application();
$application->add(new RunCommand());
$application->add(new LeaveCommand());
$application->add(new InstallCommand());

try {
    $application->run();
} catch (\Exception $exception) {
    echo sprintf('An error occurred during creator execution: %s', $exception->getMessage());
}
