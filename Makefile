all:
	@echo "Help coming soon..."

shell: ./cmd
	./cmd exec sh

run: ./cmd
	./cmd exec "/data/application.php run"

build: ./cmd
	./cmd build

phpstan: ./cmd
	./cmd exec "composer run-script phpstan"

phpcs: ./cmd
	./cmd exec "composer run-script phpcs"

phpcbf: ./cmd
	./cmd exec "composer run-script phpcbf"

quiality-check: phpstan phpcs
	@echo "Running static analysis checks"