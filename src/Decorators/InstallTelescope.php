<?php

declare(strict_types=1);

namespace Dockify\LaravelCreator\Decorators;

use Dockify\LaravelCreator\RunsProcess;

class InstallTelescope extends AbstractDecorator
{
    use RunsProcess;

    protected $confirmationPhrase = 'Would you like to install Laravel Telescope?';

    public function configure(): void
    {
        //
    }

    public function run(): void
    {
        $this->io->title('Installing Laravel Telescope');

        $this->runProcess([
            'composer', 'require', 'laravel/telescope', '--ignore-platform-reqs'
        ]);

        $this->io->success([
            'Laravel Telescope has been successfully installed.',
            'Note! This only installs telescope as composer package.',
            'In order to fully use telescope, you will need to run',
                '`make telescope-configure`, after this process finishes.'
        ]);
    }

    public function about(): void
    {
        $this->io->title('About Laravel Telescope');

        $this->io->text([
            'From official documentation:',
            'Laravel Telescope is an elegant debug assistant for the Laravel framework.',
            'Telescope provides insight into the requests coming into your application, ',
                'exceptions, log entries, database queries, queued jobs, mail, notifications, ',
                'cache operations, scheduled tasks, variable dumps and more.',
            'Telescope makes a wonderful companion to your local Laravel development environment.',
            'Read more at https://laravel.com/docs/5.8/telescope.',
        ]);
    }
}
