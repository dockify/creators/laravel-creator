<?php

declare(strict_types=1);

namespace Dockify\LaravelCreator\Decorators;

use Dockify\LaravelCreator\RunsProcess;

class InstallHorizon extends AbstractDecorator
{
    use RunsProcess;

    protected $confirmationPhrase = 'Would you like to install Laravel Horizon?';

    public function configure(): void
    {
        //
    }

    public function run(): void
    {
        $this->io->title('Installing Laravel Horizon');

        $this->runProcess([
            'composer', 'require', 'laravel/horizon', '--ignore-platform-reqs'
        ]);

        $this->io->success([
            'Laravel Horizon has been successfully installed.',
            'Note! This only installs horizon as composer package.',
            'In order to fully use horizon, you will need to take additional steps after this ' .
                'process finishes.',
            'See https://gitlab.com/dockify/laravel-creator/blob/master/README.md for more info.'
        ]);
    }

    public function about(): void
    {
        $this->io->title('About Laravel Horizon');

        $this->io->text([
            'From official documentation:',
            'Horizon provides a beautiful dashboard and code-driven configuration ' .
                'for your Laravel powered Redis queues.',
            'Horizon allows you to easily monitor key metrics of your queue system such as job ' .
                'throughput, runtime, and job failures.',
            'Read more at https://laravel.com/docs/5.8/horizon.',
        ]);
    }
}
