<?php

declare(strict_types=1);

namespace Dockify\LaravelCreator\Decorators;

use Dockify\LaravelCreator\AddsComposerScripts;
use Dockify\LaravelCreator\RunsProcess;

class InstallPhpStan extends AbstractDecorator
{
    use RunsProcess, AddsComposerScripts;

    protected $confirmationPhrase = 'Would you like to install PHPStan?';

    public function configure(): void
    {
        //
    }

    public function run(): void
    {
        $this->io->title('Installing PHPStan');

        $this->runProcess([
            'composer', 'require', '--dev', 'phpstan/phpstan'
        ]);

        $this->addComposerScripts([
            'phpstan' => 'phpstan analyse app'
        ]);
    }

    public function about(): void
    {
        $this->io->title('About PHPStan');
        $this->io->text('See https://github.com/phpstan/phpstan');
    }
}
