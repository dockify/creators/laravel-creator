<?php

declare(strict_types=1);

namespace Dockify\LaravelCreator\Decorators;

use Dockify\LaravelCreator\RunsProcess;
use Dockify\LaravelCreator\AddsComposerScripts;

class InstallCodeSniffer extends AbstractDecorator
{
    use RunsProcess, AddsComposerScripts;

    protected $confirmationPhrase = 'Would you like to install CodeSniffer?';

    public function configure(): void
    {
        //
    }

    public function run(): void
    {
        $this->io->title('Installing CodeSniffer');

        $this->runProcess([
            'composer', 'require', '--dev', 'squizlabs/php_codesniffer'
        ]);

        $this->addComposerScripts([
            'phpcs' => 'phpcs --standard=PSR1,PSR2,PSR12 app',
            'phpcbf' => 'phpcbf --standard=PSR1,PSR2,PSR12 app',
        ]);
    }

    public function about(): void
    {
        $this->io->title('About CodeSniffer');
        $this->io->text('See https://github.com/squizlabs/PHP_CodeSniffer');
    }
}
