<?php

declare(strict_types=1);

namespace Dockify\LaravelCreator\Decorators;

use Dockify\LaravelCreator\Installer;
use Dockify\LaravelCreator\RunsProcess;

class InstallLaravel extends AbstractDecorator
{
    use RunsProcess;

    const DEFAULT_PROJECT_NAME = 'DockifiedLaravelProject';

    /**
     * @var string
     */
    private $projectName;

    public function configure(): void
    {
        $this->projectName = $this->io->ask(
            'How should I name your project?',
            self::DEFAULT_PROJECT_NAME
        );

        $this->io->text(sprintf(
            '%s? Sweet name! So let\'s get started.',
            ucfirst($this->projectName)
        ));
    }

    public function run(): void
    {
        $this->io->title('Installing Laravel');

        $this->installLaravel();
    }

    protected function installLaravel()
    {
        $this->runProcess([
            'composer', 'create-project', '--prefer-dist', 'laravel/laravel',
            $this->projectName
        ]);

        chdir(sprintf(
            '%s/%s',
            Installer::PROJECT_DIR,
            $this->projectName
        ));
    }

    public function about(): void
    {
        $this->io->title('About Laravel');
        $this->io->text('See https://laravel.com/');
    }
}
