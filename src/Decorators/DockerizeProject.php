<?php

declare(strict_types=1);

namespace Dockify\LaravelCreator\Decorators;

class DockerizeProject extends AbstractDecorator
{
    protected $confirmationPhrase = 'Would you like to dockerize your application?';

    /**
     * @var int
     */
    protected $uid;

    /**
     * @var string
     */
    protected $databaseName;

    /**
     * @var string
     */
    protected $ip;

    public function configure(): void
    {
        $this->uid = (int) $this->io->ask('What is your UID?', 1000);
        $this->databaseName = $this->io->ask(
            'How should I name project database',
            'my_project'
        );
        $this->ip = $this->io->ask('What is your IP address?');
    }

    public function run(): void
    {
        $this->io->title('Dockerizing your application.');
        $this->io->text('Whalesome! Docker cuts off the necessity for installing dev tools on your host.');

        $this->copyBoilerPlates();
        $this->replaceDockerComposeVariables();
        $this->replaceEnvFileVariables();
        $this->copyDockerCompose();
        $this->changeFilePermissions();

        $this->io->success([
            'New dockified project has been successfully created!',
            'Enter your project and run `make init` to start development environment.'
        ]);
    }

    public function about(): void
    {
        $this->io->title('About Docker');
        $this->io->text([
            'Docker allows you to run your project without any other dependencies.',
            'Read more about docker at https://docker.com'
        ]);
    }

    protected function copyBoilerPlates()
    {
        shell_exec('cp -R /data/boilerplates/docker ./');
        shell_exec('mv ./docker/cmd ./');
        shell_exec('mv ./docker/Makefile ./');
        shell_exec('mv ./docker/.gitlab-ci.yml ./');
    }

    protected function replaceDockerComposeVariables()
    {
        $composeFilePath = "./docker/docker-compose.yml.example";
        $composeContent = file_get_contents($composeFilePath);

        if (!empty($composeContent)) {
            file_put_contents(
                $composeFilePath,
                str_replace(
                    '%database_name%',
                    $this->databaseName,
                    $composeContent
                )
            );
        }
    }

    protected function replaceEnvFileVariables()
    {
        $replacements = [
            '/APP_URL=.*/' => 'APP_URL=http://localhost:8000',
            '/DB_HOST=.*/' => 'DB_HOST=mysql',
            '/DB_PASSWORD=.*/' => 'DB_PASSWORD=root',
            '/DB_USERNAME=.*/' => 'DB_USERNAME=root',
            '/DB_DATABASE=.*/' => 'DB_DATABASE=' . $this->databaseName,
        ];

        $newEnvVariables = "" .
            "UID={$this->uid}\n" .
            "RUN_COMPOSER=0\n" .
            "PHP_FPM_CHDIR=public\n" .
            "NGINX_ROOT_DIR=public\n\n" .
            "XDEBUG_ENABLE=1\n" .
            "XDEBUG_PORT=9999\n" .
            "XDEBUG_IDEKEY=docker\n" .
            "XDEBUG_HANDLER=dbgp\n" .
            "XDEBUG_CONNECT_BACK=0\n" .
            "XDEBUG_HOST={$this->ip}\n"
        ;

        $envExampleFilePath = ".env.example";
        $envExampleContent = file_get_contents($envExampleFilePath);

        if (!empty($envExampleContent)) {
            $envExampleReplaced = preg_replace(
                array_keys($replacements),
                array_values($replacements),
                $envExampleContent
            );

            file_put_contents(
                $envExampleFilePath,
                "$newEnvVariables\n$envExampleReplaced"
            );
        }

        $envFilePath = ".env";
        $envContent = file_get_contents($envFilePath);

        if ($envContent) {
            $envReplaced = preg_replace(
                array_keys($replacements),
                array_values($replacements),
                $envContent
            );

            file_put_contents(
                $envFilePath,
                "$newEnvVariables\n$envReplaced"
            );
        }
    }

    protected function copyDockerCompose()
    {
        shell_exec('cp ./docker/docker-compose.yml.example ./docker-compose.yml');
    }

    protected function changeFilePermissions()
    {
        shell_exec('find . -type f -exec chmod 664 {} \;');
        shell_exec('find . -type d -exec chmod 775 {} \;');
        shell_exec('chmod -R ug+rwx storage bootstrap/cache');
        shell_exec('chmod +x cmd docker/php-fpm/before_scripts/* docker/php-fpm/common_setup.sh');
    }
}
