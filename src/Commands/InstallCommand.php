<?php

declare(strict_types=1);

namespace Dockify\LaravelCreator\Commands;

use Dockify\LaravelCreator\Decorators\InstallHorizon;
use Dockify\LaravelCreator\Decorators\InstallTelescope;
use Dockify\LaravelCreator\Installer;
use Dockify\LaravelCreator\Decorators\InstallPhpStan;
use Dockify\LaravelCreator\Decorators\InstallLaravel;
use Dockify\LaravelCreator\Decorators\DockerizeProject;
use Dockify\LaravelCreator\Decorators\InstallCodeSniffer;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InstallCommand extends Command
{
    const CMD_NAME = 'Install Laravel';

    public function configure()
    {
        $this->setName(self::CMD_NAME);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        (new Installer($io))
            ->addDecorator(new InstallLaravel($io, false))
            ->addDecorator(new InstallPhpStan($io))
            ->addDecorator(new InstallCodeSniffer($io))
            ->addDecorator(new InstallTelescope($io))
            ->addDecorator(new InstallHorizon($io))
            ->addDecorator(new DockerizeProject($io))
            ->install();
    }
}
