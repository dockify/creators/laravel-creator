<?php

declare(strict_types=1);

namespace Dockify\LaravelCreator;

trait AddsComposerScripts
{
    /**
     * @param array $scripts
     * @return void
     */
    public function addComposerScripts(array $scripts): void
    {
        $composerFileName = 'composer.json';
        $composerJsonRaw = file_get_contents($composerFileName);

        if (!empty($composerJsonRaw)) {
            $composerJson = json_decode($composerJsonRaw, true);
            $composerJson['scripts'] = array_merge(
                $composerJson['scripts'],
                $scripts
            );

            file_put_contents($composerFileName, json_encode($composerJson));
        }
    }
}
