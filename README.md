# Laravel Creator

Setup your **complete** Laravel dev environment in less than **5 minutes**. Including:

- [php](https://www.php.net/) (fpm)
- [nginx](https://www.nginx.com/) (webserver)
- [mariadb/mysql](https://mariadb.org/) (database)
- [redis](https://redis.io/) (key-value storage)
- [scheduling](https://laravel.com/docs/5.8/scheduling) (using cron)
- [laravel horizon](https://horizon.laravel.com/) (jobs)
- [laravel telescope](https://laravel.com/docs/5.8/telescope) (development dashboard)

## Prerequisites

1. [Docker](https://docker.com) installed

        $ docker --version
        Docker version 18.09.5, build e8ff056 
        
2. [Dockify](https://gitlab.com/dockify/dockify#installation) installed

        $ dockify
        Console Tool
        
        Usage:
          command [options] [arguments]
        
        Options:
          -h, --help            Display this help message
          -q, --quiet           Do not output any message
          -V, --version         Display this application version
              --ansi            Force ANSI output
              --no-ansi         Disable ANSI output
          -n, --no-interaction  Do not ask any Console Tool

3. Nothing else

    ![No other dependencies are required](./docs/img/no_dependencies_required.png)

## Usage

1. Run dockify 
        
        $ dockify use laravel
    
 2. Answer questions about your project setup
 3. That's it! You're good to go, enjoy your new dockified laravel project.
